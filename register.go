package uring

import (
	"syscall"
	"unsafe"
)

// Register flags.
const (
	IORING_REGISTER_BUFFERS = iota
	IORING_UNREGISTER_BUFFERS
	IORING_REGISTER_FILES
	IORING_UNREGISTER_FILES
	IORING_REGISTER_EVENTFD
	IORING_UNREGISTER_EVENTFD
	IORING_REGISTER_FILES_UPDATE
	IORING_REGISTER_EVENTFD_ASYNC
	IORING_REGISTER_PROBE
	IORING_REGISTER_PERSONALITY
	IORING_UNREGISTER_PERSONALITY
	IORING_REGISTER_RESTRICTIONS
	IORING_REGISTER_ENABLE_RINGS
	IORING_REGISTER_FILES2
	IORING_REGISTER_FILES_UPDATE2
	IORING_REGISTER_BUFFERS2
	IORING_REGISTER_BUFFERS_UPDATE
	IORING_REGISTER_IOWQ_AFF
	IORING_UNREGISTER_IOWQ_AFF
	IORING_REGISTER_IOWQ_MAX_WORKERS
	IORING_REGISTER_RING_FDS
	IORING_UNREGISTER_RING_FDS
	IORING_REGISTER_PBUF_RING
	IORING_UNREGISTER_PBUF_RING
	IORING_REGISTER_SYNC_CANCEL
	IORING_REGISTER_FILE_ALLOC_RANGE

	IORING_REGISTER_LAST
)

// Register files for I/O. You can use the registered files with IOSQE_FIXED_FILE flag.
func (r *Ring) RegisterFiles(fds []int32) error {
	return r.doRegister(IORING_REGISTER_FILES, uintptr(unsafe.Pointer(&fds[0])), uint32(len(fds)))
}

// UnregisterFiles unregisters all previously registered files.
func (r *Ring) UnregisterFiles() error {
	return r.doRegister(IORING_UNREGISTER_FILES, 0, 0)
}

const (
	IORING_RSRC_REGISTER_SPARSE = 1 << iota
)

type rsrcRegister struct {
	nr    uint32
	flags uint32
	resv2 uint64
	data  uint64
	tags  uint64
}

// RegisterFilesSparse registers space for count files for further use with `RegisterFilesUpdate`.
func (r *Ring) RegisterFilesSparse(count uint32) error {
	fu := rsrcRegister{
		flags: IORING_RSRC_REGISTER_SPARSE,
		nr:    uint32(count),
	}

	return r.doRegister(IORING_REGISTER_FILES2, uintptr(unsafe.Pointer(&fu)), uint32(unsafe.Sizeof(fu)))
}

type rsrcUpdate struct {
	offset uint32
	resv   uint32
	data   uint64
}

// RegisterFilesUpdate updates files for I/O.
// It can remove previously registered files (fd=-1), add new files (old fd=-1) or update existing ones.
func (r *Ring) RegisterFilesUpdate(offset uint32, fds []int32) error {
	fu := rsrcUpdate{
		offset: offset,
		data:   uint64(uintptr(unsafe.Pointer(&fds[0]))),
	}
	return r.doRegister(IORING_REGISTER_FILES_UPDATE, uintptr(unsafe.Pointer(&fu)), uint32(len(fds)))
}

// RegisterBuffers registers in-memory user buffers for I/O.
// These can be later used with WriteFixed or ReadFixed opcodes.
func (r *Ring) RegisterBuffers(iovec []syscall.Iovec) error {
	if len(iovec) == 0 {
		return nil
	}
	return r.doRegister(IORING_REGISTER_BUFFERS, uintptr(unsafe.Pointer(&iovec[0])), uint32(len(iovec)))
}

// UnregisterBuffers unregisters all previously registered buffers.
func (r *Ring) UnregisterBuffers() error {
	return r.doRegister(IORING_UNREGISTER_BUFFERS, 0, 0)
}

func (r *Ring) doRegister(op int, arg uintptr, count uint32) error {
	for {
		_, _, errno := syscall.Syscall6(
			SYS_IO_URING_REGISTER,
			uintptr(r.fd),
			uintptr(op),
			arg, uintptr(count), 0, 0)
		if errno > 0 {
			if errno == syscall.EINTR {
				continue
			}
			return errno
		}
		return nil
	}
}

// SetupEventFd creates event fd and registers it with the current uring instance.
func (r *Ring) SetupEventFd() error {
	if r.eventfd == 0 {
		r0, _, errno := syscall.Syscall(syscall.SYS_EVENTFD2, 0, 0, 0)
		if errno > 0 {
			return errno
		}
		r.eventfd = r0
	}
	for {
		_, _, errno := syscall.Syscall6(SYS_IO_URING_REGISTER, uintptr(r.fd), IORING_REGISTER_EVENTFD, uintptr(unsafe.Pointer(&r.eventfd)), 1, 0, 0)
		if errno > 0 {
			if errno == syscall.EINTR {
				continue
			}
			_ = r.CloseEventFd()
			return errno
		}
		return nil
	}
}

// CloseEventFd unregisters and closes event fd associated with the ring.
func (r *Ring) CloseEventFd() error {
	if r.eventfd == 0 {
		return nil
	}

	var errno syscall.Errno
	for {
		_, _, errno = syscall.Syscall6(SYS_IO_URING_REGISTER, uintptr(r.fd), IORING_UNREGISTER_EVENTFD, 0, 0, 0, 0)
		if errno != syscall.EINTR {
			break
		}
	}
	if err := syscall.Close(int(r.eventfd)); err != nil {
		return err
	}

	r.eventfd = 0
	if errno > 0 {
		return errno
	}
	return nil
}

// EventFd returns event fd.
func (r *Ring) EventFd() uintptr {
	return r.eventfd
}

const (
	// probeOpsSize is uintptr so that it can be passed to syscall without casting.
	probeOpsSize                 = IORING_OP_LAST
	IO_URING_OP_SUPPORTED uint16 = 1 << 0
)

// Probe ...
type Probe struct {
	LastOp Opcode
	OpsLen uint8
	resv   uint16
	resv2  [3]uint32
	Ops    [probeOpsSize]ProbeOp
}

// IsSupported returns true if operation is supported.
func (p Probe) IsSupported(op Opcode) bool {
	for i := Opcode(0); i < Opcode(p.OpsLen); i++ {
		if p.Ops[i].Op != op {
			continue
		}
		return p.Ops[i].Flags&IO_URING_OP_SUPPORTED > 0
	}
	return false
}

// ProbeOp ...
type ProbeOp struct {
	Op    Opcode
	resv  uint8
	Flags uint16
	resv2 uint32
}

func (r *Ring) RegisterProbe(p *Probe) error {
	for {
		_, _, errno := syscall.Syscall6(SYS_IO_URING_REGISTER, uintptr(r.fd), IORING_REGISTER_PROBE, uintptr(unsafe.Pointer(p)), uintptr(probeOpsSize), 0, 0)
		if errno == 0 {
			return nil
		}
		if errno != syscall.EINTR {
			return errno
		}
	}
}
