package uring

import (
	"sort"
	"syscall"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"golang.org/x/sys/unix"
)

func testTimeoutSimple(t *testing.T, r *Ring) {
	r.Submission().pushTimeout(t, timeSpec(time.Second), 42, 0, 0)

	start := time.Now()
	r.checkSubmit(t, 1, 1)
	require.True(t, time.Since(start) > time.Second)

	r.Completion().expect(t, 42, -int32(syscall.ETIME))
}

func testTimeoutAndNop(t *testing.T, r *Ring) {
	sq := r.Submission()
	sq.pushTimeout(t, timeSpec(time.Second), 0x0a, 0, 0)
	sq.pushNop(t, 0x0b)

	start := time.Now()
	r.checkSubmit(t, 1, 2)
	require.True(t, time.Since(start) < time.Second)

	cq := r.Completion()
	require.EqualValues(t, 1, cq.Len())
	cq.expect(t, 0x0b, 0)

	r.checkSubmit(t, 1, 0)
	require.True(t, time.Since(start) > time.Second)

	cq.expect(t, 0x0a, -int32(syscall.ETIME))
}

func testTimeoutCount(t *testing.T, r *Ring) {
	sq := r.Submission()
	sq.pushTimeout(t, timeSpec(time.Second), 0x0c, 1, 0)
	sq.pushNop(t, 0x0d)

	start := time.Now()
	r.checkSubmit(t, 2, 2)
	require.True(t, time.Since(start) < time.Second)

	cqes := make([]CQEntry, 2)
	require.Equal(t, 2, r.Completion().Fill(cqes))

	sort.Slice(cqes, func(i, j int) bool {
		return cqes[i].userData < cqes[j].userData
	})
	require.EqualValues(t, 0x0c, cqes[0].UserData())
	require.EqualValues(t, 0x0d, cqes[1].UserData())
	require.EqualValues(t, 0, cqes[0].Res())
	require.EqualValues(t, 0, cqes[1].Res())
}

func testTimeoutRemove(t *testing.T, r *Ring) {
	sq := r.Submission()
	sq.pushTimeout(t, timeSpec(time.Second), 0x10, 0, 0)

	r.checkSubmit(t, 0, 1)

	var sqe SQEntry
	TimeoutRemove(&sqe, 0x10)
	sqe.SetUserData(0x11)
	require.True(t, sq.Push(sqe))

	start := time.Now()
	r.checkSubmit(t, 2, 1)
	require.True(t, time.Since(start) < time.Second)

	cqes := make([]CQEntry, 2)
	require.Equal(t, 2, r.Completion().Fill(cqes))
	sort.Slice(cqes, func(i, j int) bool {
		return cqes[i].userData < cqes[j].userData
	})

	require.EqualValues(t, 0x10, cqes[0].UserData())
	require.EqualValues(t, 0x11, cqes[1].UserData())
	require.EqualValues(t, -int32(syscall.ECANCELED), cqes[0].Res())
	require.EqualValues(t, 0, cqes[1].Res())
}

func testTimeoutCancel(t *testing.T, r *Ring) {
	sq := r.Submission()
	sq.pushTimeout(t, timeSpec(time.Second), 0x10, 0, 0)

	r.checkSubmit(t, 0, 1)

	var sqe SQEntry
	AsyncCancel(&sqe, 0x10)
	sqe.SetUserData(0x11)
	require.True(t, sq.Push(sqe))

	start := time.Now()
	r.checkSubmit(t, 2, 1)
	require.True(t, time.Since(start) < time.Second)

	cqes := make([]CQEntry, 2)
	require.Equal(t, 2, r.Completion().Fill(cqes))
	sort.Slice(cqes, func(i, j int) bool {
		return cqes[i].userData < cqes[j].userData
	})

	require.EqualValues(t, 0x10, cqes[0].UserData())
	require.EqualValues(t, 0x11, cqes[1].UserData())
	require.EqualValues(t, -int32(syscall.ECANCELED), cqes[0].Res())
	require.EqualValues(t, 0, cqes[1].Res())
}

func testTimeoutAbsolute(t *testing.T, r *Ring) {
	var ts unix.Timespec
	require.NoError(t, unix.ClockGettime(unix.CLOCK_REALTIME, &ts))
	ts.Sec += 3

	start := time.Now()
	r.Submission().pushTimeout(t, &ts, 0x10, 0, IORING_TIMEOUT_ABS|IORING_TIMEOUT_REALTIME)
	r.checkSubmit(t, 4, 1)
	require.True(t, time.Since(start) > time.Second)

	r.Completion().expect(t, 0x10, -int32(syscall.ETIME))
}

func testLinkTimeoutCanceled(t *testing.T, r *Ring) {
	var sqeN SQEntry
	Nop(&sqeN)
	sqeN.SetUserData(0x20)
	sqeN.SetFlags(IOSQE_IO_LINK)

	var sqe SQEntry
	LinkTimeout(&sqe, timeSpec(time.Second), 0, 0)
	sqe.SetUserData(0x21)

	sq := r.Submission()
	require.True(t, sq.Push(sqeN), "queue is full")
	require.True(t, sq.Push(sqe), "queue is full")

	start := time.Now()
	r.checkSubmit(t, 2, 2)
	require.True(t, time.Since(start) < time.Second)

	cqes := make([]CQEntry, 2)
	require.EqualValues(t, 2, r.Completion().Fill(cqes))
	require.EqualValues(t, 0x20, cqes[0].UserData())
	require.EqualValues(t, 0x21, cqes[1].UserData())
	require.EqualValues(t, 0, cqes[0].Res())
	require.EqualValues(t, -int32(syscall.ECANCELED), cqes[1].Res())
}

func testLinkTimeoutFired(t *testing.T, r *Ring) {
	var sqeT SQEntry
	Timeout(&sqeT, timeSpec(time.Second*2), 0, 0)
	sqeT.SetUserData(0x30)
	sqeT.SetFlags(IOSQE_IO_LINK)

	var sqe SQEntry
	LinkTimeout(&sqe, timeSpec(time.Second), 0, 0)
	sqe.SetUserData(0x31)

	sq := r.Submission()
	require.True(t, sq.Push(sqeT), "queue is full")
	require.True(t, sq.Push(sqe), "queue is full")

	start := time.Now()
	r.checkSubmit(t, 2, 2)
	require.True(t, time.Since(start) > time.Second)

	cqes := make([]CQEntry, 2)
	require.EqualValues(t, 2, r.Completion().Fill(cqes))
	require.EqualValues(t, 0x31, cqes[0].UserData())
	require.EqualValues(t, 0x30, cqes[1].UserData())
	require.EqualValues(t, -int32(syscall.ETIME), cqes[0].Res())
	require.EqualValues(t, -int32(syscall.ECANCELED), cqes[1].Res())
}

func (sq *SubmissionQueue) pushTimeout(t *testing.T, ts *unix.Timespec, userData uint64, count uint64, flags FlagOpTimeout) {
	var sqe SQEntry
	Timeout(&sqe, ts, count, flags)
	sqe.SetUserData(userData)
	require.True(t, sq.Push(sqe), "queue is full")
}
