package uring

import (
	"syscall"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"golang.org/x/sys/unix"
)

type ringTestContext struct {
	r     *Ring
	probe *Probe
}

func newRingTestContext(t *testing.T) ringTestContext {
	r, err := Setup(10, nil)
	require.NoError(t, err)
	t.Cleanup(func() { require.NoError(t, r.Close()) })

	c := ringTestContext{r: r}

	p := new(Probe)
	if err := r.RegisterProbe(p); err != nil {
		t.Logf("failed to register probe: %v", err)
		t.Logf("some tests are expected to fail on old kernels")
	} else {
		c.probe = p
	}

	return c
}

// TODO include `mustSupport` checks in every test to allow passing this on older kernels.
// func mustSupport(t *testing.T, p *Probe, op Opcode) {
// 	if p != nil && !p.IsSupported(op) {
// 		t.Skipf("opcode %d is unsupported, skipped", op)
// 	}
// }

func (c ringTestContext) run(t *testing.T, name string, f func(*testing.T, *Ring)) {
	// Sync to avoid failed `Submit` because kernel thinks there are unconsumed entries in the completion queue.
	c.r.cq.Sync()
	t.Run(name, func(t *testing.T) {
		f(t, c.r)
	})
}

func writeRead(t *testing.T, r *Ring, fdIn, fdOut uintptr, flags FlagSQE) {
	expected := []byte("The quick brown fox jumps over the lazy dog.")
	actual := make([]byte, len(expected))

	var sqeW SQEntry
	Write(&sqeW, fdIn, expected)
	sqeW.SetUserData(1)
	sqeW.SetFlags(IOSQE_IO_LINK | flags)

	var sqeR SQEntry
	Read(&sqeR, fdOut, actual)
	sqeR.SetUserData(2)
	sqeR.SetFlags(flags)

	sq := r.Submission()
	require.True(t, sq.PushMany([]SQEntry{sqeW, sqeR}), "queue is full")
	r.checkSubmit(t, 2, 2)

	cqes := make([]CQEntry, 2)
	require.Equal(t, 2, r.Completion().Fill(cqes))
	require.EqualValues(t, 1, cqes[0].UserData())
	require.EqualValues(t, 2, cqes[1].UserData())
	require.EqualValues(t, len(expected), cqes[0].Res())
	require.EqualValues(t, len(actual), cqes[1].Res())
	require.Equal(t, expected, actual)
}

func sendRecv(t *testing.T, r *Ring, fdIn, fdOut uintptr, flags FlagSQE) {
	expected := []byte("The quick brown fox jumps over the lazy dog.")
	actual := make([]byte, len(expected))

	var sqeW SQEntry
	Send(&sqeW, fdIn, expected, 0)
	sqeW.SetUserData(1)
	sqeW.SetFlags(IOSQE_IO_LINK | flags)

	var sqeR SQEntry
	Recv(&sqeR, fdOut, actual, 0)
	sqeR.SetUserData(2)
	sqeR.SetFlags(flags)

	sq := r.Submission()
	require.True(t, sq.PushMany([]SQEntry{sqeW, sqeR}), "queue is full")
	r.checkSubmit(t, 2, 2)

	cqes := make([]CQEntry, 2)
	require.Equal(t, 2, r.Completion().Fill(cqes))
	require.EqualValues(t, 1, cqes[0].UserData())
	require.EqualValues(t, 2, cqes[1].UserData())
	require.EqualValues(t, len(expected), cqes[0].Res())
	require.EqualValues(t, len(actual), cqes[1].Res())
	require.Equal(t, expected, actual)
}

func writevReadv(t *testing.T, r *Ring, fdIn, fdOut uintptr) {
	expected1 := []byte("The quick brown fox jumps over the lazy dog.")
	expected2 := []byte("Это просто текст.")

	actual1 := make([]byte, len(expected1))
	actual2 := make([]byte, len(expected2))

	expected3 := []syscall.Iovec{
		toIovec(expected1),
		toIovec(expected2),
	}
	actual3 := []syscall.Iovec{
		toIovec(actual1),
		toIovec(actual2),
	}

	var sqeW SQEntry
	Writev(&sqeW, fdIn, expected3, 0)
	sqeW.SetUserData(1)
	sqeW.SetFlags(IOSQE_IO_LINK)

	var sqeR SQEntry
	Readv(&sqeR, fdOut, actual3, 0)
	sqeR.SetUserData(2)

	sq := r.Submission()
	require.True(t, sq.Push(sqeW))
	require.True(t, sq.Push(sqeR))

	r.checkSubmit(t, 2, 2)

	cqes := make([]CQEntry, 2)
	require.Equal(t, 2, r.Completion().Fill(cqes))
	require.EqualValues(t, 1, cqes[0].UserData())
	require.EqualValues(t, 2, cqes[1].UserData())
	require.EqualValues(t, len(expected1)+len(expected2), cqes[0].Res())
	require.EqualValues(t, len(actual1)+len(actual2), cqes[1].Res())
	require.Equal(t, expected1, actual1)
	require.Equal(t, expected2, actual2)
}

func toIovec(data []byte) syscall.Iovec {
	return syscall.Iovec{
		Base: &data[0],
		Len:  uint64(len(data)),
	}
}

func timeSpec(d time.Duration) *unix.Timespec {
	ts := unix.NsecToTimespec(int64(d))
	return &ts
}

func (cq *CompletionQueue) expect(t *testing.T, userData uint64, res int32) {
	cqe, ok := cq.GetEntry()
	require.True(t, ok, "completion queue is empty")
	require.Equal(t, userData, cqe.UserData(), "unexpected userData")
	require.Equal(t, res, cqe.Res(), "unexpected operation result: %s", -syscall.Errno(cqe.Res()))

	cq.Sync() // Notify kernel of consumed CQE.
}

func (sq *SubmissionQueue) pushNop(t *testing.T, userData uint64) {
	var sqe SQEntry
	Nop(&sqe)
	sqe.SetUserData(userData)
	require.True(t, sq.Push(sqe), "queue is full")
}

func (r *Ring) checkSubmit(t *testing.T, want uint32, submitted uint32) {
	r.sq.Sync()

	actual, err := r.SubmitAndWait(want)
	require.NoError(t, err)
	require.Equal(t, submitted, actual)

	r.cq.Sync() // Fetch just apperared CQEs in userspace.
}
