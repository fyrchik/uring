// Package netutil contains sockaddr structs from the syscall package.
// The only difference is that Sockaddr() method is exported and can be used
// for creating SQEs.
package netutil

import (
	"net"
	"syscall"
	"unsafe"
)

type Sockaddr interface {
	Sockaddr() (ptr unsafe.Pointer, ln uint32)
}

type SockaddrInet4 struct {
	Port int
	Addr [4]byte
	raw  syscall.RawSockaddrInet4
}

func (sa *SockaddrInet4) Sockaddr() (unsafe.Pointer, uint32) {
	sa.raw.Family = syscall.AF_INET
	p := (*[2]byte)(unsafe.Pointer(&sa.raw.Port))
	p[0] = byte(sa.Port >> 8)
	p[1] = byte(sa.Port)
	sa.raw.Addr = sa.Addr
	return unsafe.Pointer(&sa.raw), syscall.SizeofSockaddrInet4
}

func (sa *SockaddrInet4) ToSyscall() *syscall.SockaddrInet4 {
	return (*syscall.SockaddrInet4)(unsafe.Pointer(sa))
}

func FromSyscallInet4(ssa *syscall.SockaddrInet4) *SockaddrInet4 {
	return (*SockaddrInet4)(unsafe.Pointer(ssa))
}

type SockaddrInet6 struct {
	Port   int
	ZoneID uint32
	Addr   [16]byte
	raw    syscall.RawSockaddrInet6
}

func (sa *SockaddrInet6) Sockaddr() (unsafe.Pointer, uint32) {
	sa.raw.Family = syscall.AF_INET6
	p := (*[2]byte)(unsafe.Pointer(&sa.raw.Port))
	p[0] = byte(sa.Port >> 8)
	p[1] = byte(sa.Port)
	sa.raw.Scope_id = sa.ZoneID
	sa.raw.Addr = sa.Addr
	return unsafe.Pointer(&sa.raw), syscall.SizeofSockaddrInet6
}

func (sa *SockaddrInet6) ToSyscall() *syscall.SockaddrInet6 {
	return (*syscall.SockaddrInet6)(unsafe.Pointer(sa))
}

func FromSyscallInet6(ssa *syscall.SockaddrInet6) *SockaddrInet6 {
	return (*SockaddrInet6)(unsafe.Pointer(ssa))
}

func TCPAddrToSockaddr(addr *net.TCPAddr) Sockaddr {
	if ip := addr.IP.To4(); ip != nil {
		return &SockaddrInet4{
			Addr: [4]byte(addr.IP),
			Port: addr.Port,
		}
	}
	if ip := addr.IP.To16(); ip != nil {
		return &SockaddrInet6{
			Addr: [16]byte(addr.IP),
			Port: addr.Port,
		}
	}
	return nil
}
