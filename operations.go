package uring

import (
	"math"
	"syscall"
	"unsafe"

	"codeberg.org/fyrchik/uring/netutil"
	"golang.org/x/sys/unix"
)

// Opcode is the type for uring opcodes.
type Opcode uint8

// Opcodes.
const (
	IORING_OP_NOP Opcode = iota
	IORING_OP_READV
	IORING_OP_WRITEV
	IORING_OP_FSYNC
	IORING_OP_READ_FIXED
	IORING_OP_WRITE_FIXED
	IORING_OP_POLL_ADD    // unimplemented
	IORING_OP_POLL_REMOVE // unimplemented
	IORING_OP_SYNC_FILE_RANGE
	IORING_OP_SENDMSG
	IORING_OP_RECVMSG
	IORING_OP_TIMEOUT
	IORING_OP_TIMEOUT_REMOVE
	IORING_OP_ACCEPT
	IORING_OP_ASYNC_CANCEL
	IORING_OP_LINK_TIMEOUT
	IORING_OP_CONNECT
	IORING_OP_FALLOCATE
	IORING_OP_OPENAT
	IORING_OP_CLOSE
	IORING_OP_FILES_UPDATE // unimplemented
	IORING_OP_STATX        // unimplemented
	IORING_OP_READ
	IORING_OP_WRITE
	IORING_OP_FADVISE // unimplemented
	IORING_OP_MADVISE // unimplemented
	IORING_OP_SEND
	IORING_OP_RECV
	IORING_OP_OPENAT2
	IORING_OP_EPOLL_CTL // unimplemented
	IORING_OP_SPLICE
	IORING_OP_PROVIDE_BUFFERS // unimplemented
	IORING_OP_REMOVE_BUFFERS  // unimplemented
	IORING_OP_TEE
	IORING_OP_SHUTDOWN  // unimplemented
	IORING_OP_RENAMEAT  // unimplemented
	IORING_OP_UNLINKAT  // unimplemented
	IORING_OP_MKDIRAT   // unimplemented
	IORING_OP_SYMLINKAT // unimplemented
	IORING_OP_LINKAT    // unimplemented
	IORING_OP_MSG_RING  // unimplemented
	IORING_OP_FSETXATTR // unimplemented
	IORING_OP_SETXATTR  // unimplemented
	IORING_OP_FGETXATTR // unimplemented
	IORING_OP_GETXATTR  // unimplemented
	IORING_OP_SOCKET    // unimplemented
	IORING_OP_URING_CMD // unimplemented
	IORING_OP_SEND_ZC
	IORING_OP_SENDMSG_ZC

	IORING_OP_LAST
)

// FlagOpTimeout is the type for timeout opcode flags.
type FlagOpTimeout uint32

// SQE timeout flags.
const (
	IORING_TIMEOUT_ABS FlagOpTimeout = 1 << iota
	IORING_TIMEOUT_UPDATE
	IORING_TIMEOUT_BOOTTIME
	IORING_TIMEOUT_REALTIME
	IORING_LINK_TIMEOUT_UPDATE
	IORING_TIMEOUT_ETIME_SUCCESS

	IORING_TIMEOUT_CLOCK_MASK  = (IORING_TIMEOUT_BOOTTIME | IORING_TIMEOUT_REALTIME)
	IORING_TIMEOUT_UPDATE_MASK = (IORING_TIMEOUT_UPDATE | IORING_LINK_TIMEOUT_UPDATE)
)

// FlagOpRecvSend is the type for send/recv opcode modifiers.
// Note thet these are uring-specific, normal MSG_* flags are provided separately.
type FlagOpRecvSend uint16

// SQE send/recv modifiers.
const (
	IORING_RECVSEND_POLL_FIRST FlagOpRecvSend = 1 << iota
	IORING_RECV_MULTISHOT
	IORING_RECVSEND_FIXED_BUF
	IORING_SEND_ZC_REPORT_USAGE
)

// FlagOpSplice is the type for splice/tee opcode flags.
// For other possible flags, see splice(2).
type FlagOpSplice uint32

const (
	SPLICE_F_FD_IN_FIXED FlagOpSplice = 1 << 31
)

// FlagOpFsync is the type for fsync opcode flags.
type FlagOpFsync uint32

const (
	IORING_FSYNC_DATASYNC FlagOpFsync = 1 << iota
)

func Openat(sqe *SQEntry, dirfd int32, pathptr *byte, flags uint32, mode uint32) {
	sqe.opcode = IORING_OP_OPENAT
	sqe.fd = dirfd
	sqe.opcodeFlags = flags
	sqe.addr = (uint64)(uintptr(unsafe.Pointer(pathptr)))
	sqe.len = mode
}

func Openat2(sqe *SQEntry, dirfd int32, pathptr *byte, how unix.OpenHow) {
	sqe.opcode = IORING_OP_OPENAT2
	sqe.fd = dirfd
	sqe.addr = (uint64)(uintptr(unsafe.Pointer(pathptr)))
	sqe.len = uint32(unsafe.Sizeof(unix.OpenHow{}))
	sqe.offset = uint64(escapes(uintptr(unsafe.Pointer(&how))))
}

func Openat2Direct(sqe *SQEntry, dirfd int32, pathptr *byte, how unix.OpenHow, fileIndex int32) {
	sqe.opcode = IORING_OP_OPENAT2
	sqe.fd = dirfd
	sqe.addr = (uint64)(escapes(uintptr(unsafe.Pointer(pathptr))))
	sqe.len = uint32(unsafe.Sizeof(unix.OpenHow{}))
	sqe.offset = uint64(escapes(uintptr(unsafe.Pointer(&how))))
	if fileIndex >= 0 {
		setTargetFixedFile(sqe, fileIndex)
	} else {
		sqe._pad3 = math.MaxUint32
	}
}

func setTargetFixedFile(sqe *SQEntry, index int32) {
	// 0 means no fixed files, indexes should be encoded as "index + 1"
	sqe._pad3 = uint32(index + 1)
}

//go:uintptrescapes
func escapes(p uintptr) uintptr {
	return p
}

func Close(sqe *SQEntry, fd uintptr) {
	sqe.opcode = IORING_OP_CLOSE
	sqe.fd = int32(fd)
}

func CloseDirect(sqe *SQEntry, fileIndex int32) {
	sqe.opcode = IORING_OP_CLOSE
	setTargetFixedFile(sqe, fileIndex)
}

func Nop(sqe *SQEntry) {
	sqe.opcode = IORING_OP_NOP
}

func Write(sqe *SQEntry, fd uintptr, buf []byte) {
	WriteAt(sqe, fd, buf, 0)
}

func Read(sqe *SQEntry, fd uintptr, buf []byte) {
	ReadAt(sqe, fd, buf, 0)
}

func WriteAt(sqe *SQEntry, fd uintptr, buf []byte, offset uint64) {
	rw(sqe, IORING_OP_WRITE, fd, uintptr(unsafe.Pointer(&buf[0])), len(buf), offset)
}

func ReadAt(sqe *SQEntry, fd uintptr, buf []byte, offset uint64) {
	rw(sqe, IORING_OP_READ, fd, uintptr(unsafe.Pointer(&buf[0])), len(buf), offset)
}

func Readv(sqe *SQEntry, fd uintptr, iovec []syscall.Iovec, offset uint64) {
	rw(sqe, IORING_OP_READV, fd, uintptr(unsafe.Pointer(&iovec[0])), len(iovec), offset)
}

func Writev(sqe *SQEntry, fd uintptr, iovec []syscall.Iovec, offset uint64) {
	rw(sqe, IORING_OP_WRITEV, fd, uintptr(unsafe.Pointer(&iovec[0])), len(iovec), offset)
}

func WriteFixed(sqe *SQEntry, fd uintptr, buf []byte, offset uint64, bufIndex int) {
	rw(sqe, IORING_OP_WRITE_FIXED, fd, uintptr(unsafe.Pointer(&buf[0])), len(buf), offset)
	sqe.bufIndex = uint16(bufIndex)
}

func ReadFixed(sqe *SQEntry, fd uintptr, buf []byte, offset uint64, bufIndex int) {
	rw(sqe, IORING_OP_READ_FIXED, fd, uintptr(unsafe.Pointer(&buf[0])), len(buf), offset)
	sqe.bufIndex = uint16(bufIndex)
}

func Send(sqe *SQEntry, sockFd uintptr, buf []byte, flags uint32) {
	rw(sqe, IORING_OP_SEND, sockFd, uintptr(unsafe.Pointer(&buf[0])), len(buf), 0)
	sqe.opcodeFlags = flags
}

func Recv(sqe *SQEntry, sockFd uintptr, buf []byte, flags uint32) {
	rw(sqe, IORING_OP_RECV, sockFd, uintptr(unsafe.Pointer(&buf[0])), len(buf), 0)
	sqe.opcodeFlags = flags
}

func SendMsg(sqe *SQEntry, sockFd uintptr, msg *syscall.Msghdr, flags uint32) {
	rw(sqe, IORING_OP_SENDMSG, sockFd, uintptr(unsafe.Pointer(msg)), 1, 0)
	sqe.opcodeFlags = flags
}

func RecvMsg(sqe *SQEntry, sockFd uintptr, msg *syscall.Msghdr, flags uint32) {
	rw(sqe, IORING_OP_RECVMSG, sockFd, uintptr(unsafe.Pointer(msg)), 1, 0)
	sqe.opcodeFlags = flags
}

func SendZc(sqe *SQEntry, sockFd uintptr, buf []byte, flags uint32, zcFlags FlagOpRecvSend) {
	rw(sqe, IORING_OP_SEND_ZC, sockFd, uintptr(unsafe.Pointer(&buf[0])), len(buf), 0)
	sqe.opcodeFlags = flags
	sqe.priority = uint16(zcFlags)
}

func SendZcFixed(sqe *SQEntry, sockFd uintptr, buf []byte, flags uint32, zcFlags FlagOpRecvSend, bufIndex uint16) {
	SendZc(sqe, sockFd, buf, flags, zcFlags)
	sqe.priority |= uint16(IORING_RECVSEND_FIXED_BUF)
	sqe.bufIndex = bufIndex
}

func SendMsgZc(sqe *SQEntry, sockFd uintptr, msg *syscall.Msghdr, flags uint32) {
	SendMsg(sqe, sockFd, msg, flags)
	sqe.opcode = IORING_OP_SENDMSG_ZC
}

func rw(sqe *SQEntry, op Opcode, fd uintptr, addr uintptr, length int, offset uint64) {
	sqe.opcode = op
	sqe.fd = int32(fd)
	sqe.offset = offset
	sqe.addr = uint64(addr)
	sqe.len = uint32(length)
}

func Timeout(sqe *SQEntry, ts *unix.Timespec, count uint64, flags FlagOpTimeout) {
	sqe.fd = -1
	sqe.opcode = IORING_OP_TIMEOUT
	sqe.addr = uint64(uintptr(unsafe.Pointer(ts)))
	sqe.len = 1
	sqe.offset = count
	sqe.opcodeFlags = uint32(flags)
}

func LinkTimeout(sqe *SQEntry, ts *unix.Timespec, count uint64, flags FlagOpTimeout) {
	sqe.fd = -1
	sqe.opcode = IORING_OP_LINK_TIMEOUT
	sqe.addr = uint64(uintptr(unsafe.Pointer(ts)))
	sqe.len = 1
	sqe.offset = count
	sqe.opcodeFlags = uint32(flags)
}

func TimeoutRemove(sqe *SQEntry, userData uint64) {
	sqe.fd = -1
	sqe.opcode = IORING_OP_TIMEOUT_REMOVE
	sqe.addr = userData
}

func AsyncCancel(sqe *SQEntry, userData uint64) {
	sqe.fd = -1
	sqe.opcode = IORING_OP_ASYNC_CANCEL
	sqe.addr = userData
}

func Fallocate(sqe *SQEntry, fd uintptr, mode int, offset, length uint64) {
	rw(sqe, IORING_OP_FALLOCATE, fd, 0, mode, offset)
	sqe.addr = length
}

func Fsync(sqe *SQEntry, fd uintptr, flags FlagOpFsync) {
	sqe.opcode = IORING_OP_FSYNC
	sqe.fd = int32(fd)
	sqe.opcodeFlags = uint32(flags)
}

func SyncFileRange(sqe *SQEntry, fd uintptr, offset int64, ln int, flags uint32) {
	sqe.opcode = IORING_OP_SYNC_FILE_RANGE
	sqe.fd = int32(fd)
	sqe.offset = uint64(offset)
	sqe.len = uint32(ln)
	sqe.opcodeFlags = flags
}

func Accept(sqe *SQEntry, fd uintptr, sa netutil.Sockaddr, flags uint32) {
	ptr, n := sa.Sockaddr()
	rw(sqe, IORING_OP_ACCEPT, fd, uintptr(ptr), 0, uint64(escapes(uintptr(unsafe.Pointer(&n)))))
	sqe.opcodeFlags = flags
}

func Connect(sqe *SQEntry, fd uintptr, sa netutil.Sockaddr) {
	ptr, n := sa.Sockaddr()
	rw(sqe, IORING_OP_CONNECT, fd, uintptr(ptr), 0, uint64(n))
}

func Splice(sqe *SQEntry, fdIn uintptr, offIn int64, fdOut uintptr, offOut int64, n int, flags FlagOpSplice) {
	sqe.opcode = IORING_OP_SPLICE
	sqe.fd = int32(fdOut)
	sqe.len = uint32(n)
	sqe.offset = uint64(offOut)
	sqe._pad3 = uint32(fdIn)
	sqe.addr = uint64(offIn)
	sqe.opcodeFlags = uint32(flags)
}

func Tee(sqe *SQEntry, rfd uintptr, wfd uintptr, ln int, flags FlagOpSplice) {
	sqe.opcode = IORING_OP_TEE
	sqe.fd = int32(wfd)
	sqe.len = uint32(ln)
	sqe.opcodeFlags = uint32(flags)
	sqe._pad3 = uint32(rfd)
}
