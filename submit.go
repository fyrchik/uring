package uring

import (
	"sync/atomic"
	"syscall"
)

// FlagEnter is a type for `io_uring_enter` syscall flags.
type FlagEnter uint32

// Enter flags.
const (
	IORING_ENTER_GETEVENTS FlagEnter = 1 << iota
	IORING_ENTER_SQ_WAKEUP
)

// FlagSQE is the type for non opcode-specifig SQE flags.
type FlagSQE uint8

// Submission queue entry flags.
const (
	IOSQE_FIXED_FILE FlagSQE = 1 << iota
	IOSQE_IO_DRAIN
	IOSQE_IO_LINK
	IOSQE_IO_HARDLINK
	IOSQE_ASYNC
	IOSQE_BUFFER_SELECT
)

// Submit submits acquired SQEs to the kernel. Returns number of entries submitted.
func (r *Ring) Submit() (uint32, error) {
	return r.SubmitAndWait(0)
}

// SubmitAndWait submits acquired SQEs to the kernel. Returns number of entries submitted.
func (r *Ring) SubmitAndWait(want uint32) (uint32, error) {
	return r.submit(want)
}

func (r *Ring) submit(want uint32) (uint32, error) {
	head := atomic.LoadUint32(r.sq.kHead)
	tail := atomic.LoadUint32(r.sq.kTail)
	submitted := tail - head

	var flags FlagEnter
	if want > 0 || r.flags&IORING_SETUP_IOPOLL != 0 || atomic.LoadUint32(r.sq.kFlags)&IORING_SQ_CQ_OVERFLOW != 0 {
		flags |= IORING_ENTER_GETEVENTS
	}

	if r.flags&IORING_SETUP_SQPOLL != 0 {
		if atomic.LoadUint32(r.sq.kFlags)&IORING_SQ_NEED_WAKEUP != 0 {
			flags |= IORING_ENTER_SQ_WAKEUP
		} else if want == 0 {
			return submitted, nil
		}
	}

	for {
		n, err := r.enter(submitted, want, flags)
		if err == syscall.EINTR {
			continue
		}
		return n, err
	}
}

// enter performs `io_uring_enter` syscall on the ring file descriptor, using provided parameters.
func (r *Ring) enter(submitted uint32, minComplete uint32, flags FlagEnter) (uint32, error) {
	var (
		r1    uintptr
		errno syscall.Errno
	)
	r1, _, errno = syscall.Syscall6(SYS_IO_URING_ENTER, uintptr(r.fd), uintptr(submitted), uintptr(minComplete), uintptr(flags), 0, 0)
	if errno == 0 {
		return uint32(r1), nil
	}
	return uint32(r1), error(errno)
}
