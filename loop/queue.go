package loop

import (
	"math"
	"runtime"
	"sync"
	"syscall"
	"time"

	"codeberg.org/fyrchik/uring"
)

type queue struct {
	ring *uring.Ring

	wg      sync.WaitGroup
	closed  bool
	closeCh chan struct{}

	nonces  chan int
	results []result

	submissionTimer time.Duration
	submitLimit     uint32
	submitEvent     *sync.Cond
}

type result struct {
	ch chan struct{}
	uring.CQEntry
}

const closed = 1 << 63

func (q *queue) init(r *uring.Ring, lp *Params) {
	results := make([]result, r.SQSize())
	nonces := make(chan int, r.SQSize())
	for i := range results {
		results[i].ch = make(chan struct{}, 1)
		nonces <- i
	}

	q.results = results
	q.nonces = nonces
	q.ring = r
	q.submissionTimer = lp.SubmissionTimer
	q.submitEvent = sync.NewCond(new(sync.Mutex))
	q.submitLimit = r.SQSize()
	q.closeCh = make(chan struct{})
	q.wg.Add(1)
	go q.submitLoop()
}

func (q *queue) completionLoop() {
	defer q.wg.Done()
	for q.tryComplete() {
	}
}

func (q *queue) Complete(sqe uring.SQEntry) (uring.CQEntry, error) {
	nonce, err := q.getFree()
	if err != nil {
		return uring.CQEntry{}, err
	}

	sqe.SetUserData(uint64(nonce))
	//atomic.StoreUint32(&l.results[nonce].used, 1)
	err = q.submit(sqe)
	if err != nil {
		q.markFree(nonce)
		return uring.CQEntry{}, err
	}

	result := &q.results[nonce]
	_, ok := <-result.ch
	if !ok {
		// No need to mark it as free, the queue is closing.
		return uring.CQEntry{}, ErrClosed
	}
	assert(result.CQEntry.UserData() == uint64(nonce),
		"userData does not match: %x != %x",
		result.CQEntry.UserData(), uint64(nonce))

	if r := result.CQEntry.Res(); r < 0 {
		err = syscall.Errno(-r)
	}

	// The order is important: we must not read from result after marking it free.
	cqe := result.CQEntry
	q.markFree(nonce)
	return cqe, err
}

func (q *queue) submit(sqe uring.SQEntry) (err error) {
	q.submitEvent.L.Lock()
	sq := q.ring.Submission()
	for !sq.Push(sqe) && !q.closed {
		q.submitEvent.Signal()
		sq.Sync()
	}
	if q.closed {
		err = ErrClosed
	}
	q.submitEvent.L.Unlock()

	return err
}

func (q *queue) getFree() (int, error) {
	return <-q.nonces, nil
}

func (q *queue) markFree(index int) {
	assert(len(q.nonces) < cap(q.nonces), "expected nonces channel not to be full")
	q.nonces <- index
}

func (q *queue) tryComplete() bool {
	cq := q.ring.Completion()

	for cqe, ok := cq.GetEntry(); ok; cqe, ok = cq.GetEntry() {
		if cqe.UserData() == math.MaxUint64 {
			continue
		}

		if cqe.UserData()&closed > 0 {
			return false
		}

		req := &q.results[cqe.UserData()%uint64(len(q.results))]
		req.CQEntry = cqe
		req.ch <- struct{}{}
	}
	assert(cq.Len() == 0, "expected completion queue to be empty after being consumed")
	return true
}

func (q *queue) submitLoop() {
	defer q.wg.Done()

	var (
		duration = q.submissionTimer
		timeout  = false

		timer = time.AfterFunc(duration, func() {
			q.submitEvent.L.Lock()
			timeout = true
			q.submitEvent.Signal()
			q.submitEvent.L.Unlock()
		})
	)
	defer timer.Stop()

	for {
		q.submitEvent.L.Lock()
		sq := q.ring.Submission()
		for !sq.Full() && !timeout && !q.closed {
			q.submitEvent.Wait()
			sq.Sync()
		}
		if q.closed {
			q.submitEvent.L.Unlock()
			return
		}
		sq.Sync()

		size := sq.Len()
		timed := timeout
		timeout = false
		q.submitEvent.L.Unlock()

		if size > 0 {
			_, err := q.ring.Submit()
			if err != nil {
				// Both for EBUSY and EAGAIN we should wait for completions and retry.
				// Completion queue processing is done in another goroutine, so we just
				// Goshed and continue our loop.
				if err == syscall.EBUSY || err == syscall.EAGAIN {
					runtime.Gosched()
					continue
				}
				panic(err) // FIXME unrecoverable
			}
		}
		if !timed {
			timer.Stop()
		}
		timer.Reset(duration)
	}
}

func (q *queue) Close() error {
	close(q.closeCh)

	q.submitEvent.L.Lock()
	q.closed = true
	q.submitEvent.Broadcast()
	q.submitEvent.L.Unlock()

	var sqe uring.SQEntry
	uring.Nop(&sqe)
	sqe.SetUserData(closed)
	sqe.SetFlags(uring.IOSQE_IO_DRAIN)

	q.submitEvent.L.Lock()
	sq := q.ring.Submission()
	for !sq.Push(sqe) {
		q.submitEvent.Signal()
		sq.Sync()
	}
	sq.Sync()
	q.submitEvent.L.Unlock()

	_, err := q.ring.Submit()
	if err != nil {
		return err
	}

	q.wg.Wait()

	for i := range q.results {
		close(q.results[i].ch)
	}
	return nil
}
