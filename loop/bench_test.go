package loop

import (
	"sync"
	"testing"
	"time"

	"codeberg.org/fyrchik/uring"
	"github.com/stretchr/testify/require"
)

func BenchmarkLoop(b *testing.B) {
	bench := func(b *testing.B, q *Loop) {
		var wg sync.WaitGroup
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			wg.Add(1)
			go func() {
				defer wg.Done()
				var sqe uring.SQEntry
				uring.Nop(&sqe)
				_, err := q.Complete(sqe)
				if err != nil {
					b.Error(err)
				}
			}()
		}
		wg.Wait()
	}
	b.Run("default", func(b *testing.B) {
		q, err := New(2048, &Params{
			RingCount:       1,
			SubmissionTimer: time.Microsecond * 10,
		})
		require.NoError(b, err)
		defer q.Close()
		bench(b, q)
	})
	b.Run("eventfd", func(b *testing.B) {
		q, err := New(2048, &Params{
			RingCount:   1,
			WaitEventFd: true,
		})
		require.NoError(b, err)
		defer q.Close()
		bench(b, q)
	})
}
