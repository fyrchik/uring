package loop

import (
	"fmt"
	"math/rand"
	"os"
	"path/filepath"
	"testing"
	"time"

	"codeberg.org/fyrchik/uring"
	"github.com/stretchr/testify/require"
)

func TestLoop(t *testing.T) {
	test := func(t *testing.T, l *Loop) {
		t.Cleanup(func() { l.Close() })

		fname := filepath.Join(t.TempDir(), "uring_test")
		f, err := os.OpenFile(fname, os.O_CREATE|os.O_RDWR|os.O_TRUNC, os.ModePerm)
		require.NoError(t, err)

		expected := make([]byte, 42)
		rand.Read(expected)

		var sqe uring.SQEntry

		uring.Write(&sqe, f.Fd(), expected)
		_, err = l.Complete(sqe)
		require.NoError(t, err)

		actual := make([]byte, len(expected))
		sqe.Reset()
		uring.ReadAt(&sqe, f.Fd(), actual, 0)
		_, err = l.Complete(sqe)
		require.NoError(t, err)

		require.Equal(t, expected, actual)
	}

	for _, ringCount := range []int{1, 4} {
		t.Run(fmt.Sprintf("rings=%d", ringCount), func(t *testing.T) {
			t.Run("submission timer", func(t *testing.T) {
				l, err := New(32, &Params{
					RingCount:       ringCount,
					Flags:           FlagSharedWorkers,
					SubmissionTimer: time.Microsecond * 10,
				})
				require.NoError(t, err)
				test(t, l)
			})
			t.Run("eventfd", func(t *testing.T) {
				l, err := New(32, &Params{
					RingCount:   ringCount,
					Flags:       FlagSharedWorkers,
					WaitEventFd: true,
				})
				require.NoError(t, err)
				test(t, l)
			})
		})
	}

}
