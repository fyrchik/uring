package loop

import (
	"errors"
	"fmt"
	"sync"
	"syscall"
	"time"

	"codeberg.org/fyrchik/uring"
)

type Loop struct {
	queues    []queue
	byEventfd map[int32]int
	poll      poll
	wg        sync.WaitGroup
}

var (
	// ErrClosed is returned after uring is closed.
	ErrClosed = errors.New("uring: closed")
	// ErrBusy is returned when submission queue is full.
	ErrBusy = errors.New("uring: no available submission entries")
)

// Flags alter high-level runtime loop behaviour.
type Flags uint

const (
	// FlagSharedWorkers allows to shared worker pool between all rings.
	FlagSharedWorkers Flags = 1 << iota
)

// Params represents high-level queue params.
type Params struct {
	SubmissionTimer time.Duration
	WaitEventFd     bool
	Flags           Flags
	RingCount       int
	RingParams      uring.Params
}

func defaultParams() *Params {
	return &Params{
		RingCount:       1,
		WaitEventFd:     true,
		Flags:           FlagSharedWorkers,
		SubmissionTimer: 10 * time.Microsecond,
	}
}

// New returns new Loop.
func New(size int, lp *Params) (*Loop, error) {
	if lp == nil {
		lp = defaultParams()
	}

	l := &Loop{queues: make([]queue, lp.RingCount)}
	for i := 0; i < lp.RingCount; i++ {
		p := *lp
		if p.Flags&FlagSharedWorkers != 0 && i > 0 {
			p.RingParams.Flags |= uring.IORING_SETUP_ATTACH_WQ
			p.RingParams.WQFd = uint32(l.queues[0].ring.Fd())
		}
		ring, err := uring.Setup(size, &p.RingParams)
		if err != nil {
			return nil, err
		}
		l.queues[i].init(ring, &p)
	}

	if lp.WaitEventFd {
		if err := l.poll.init(len(l.queues)); err != nil {
			return nil, fmt.Errorf("failed to init epoll: %w", err)
		}

		byEventfd := make(map[int32]int, len(l.queues))
		for i := range l.queues {
			r := l.queues[i].ring
			for {
				if err := r.SetupEventFd(); err != nil {
					if err == syscall.EINTR {
						continue
					}
					return nil, fmt.Errorf("failed to setup eventfd: %w", err)
				}
				break
			}
			if err := l.poll.addRead(int32(r.EventFd())); err != nil {
				return nil, fmt.Errorf("failed to add eventfd to epoll: %w", err)
			}
			byEventfd[int32(r.EventFd())] = i
		}

		l.byEventfd = byEventfd
		l.wg.Add(1)
		go l.epollLoop()
	} else {
		for i := range l.queues {
			l.queues[i].wg.Add(1)
			go l.queues[i].completionLoop()
		}
	}
	return l, nil
}

func (l *Loop) Complete(sqe uring.SQEntry) (uring.CQEntry, error) {
	return l.queues[l.getQueue()].Complete(sqe)
}

func (l *Loop) getQueue() int {
	if len(l.queues) == 1 {
		return 0
	}
	tid := int(syscall.Gettid())
	return tid % len(l.queues)
}

func (l *Loop) Close() error {
	var firstErr error
	for i := range l.queues {
		if err := l.queues[i].Close(); err != nil && firstErr == nil {
			firstErr = err
		}
	}
	l.wg.Wait()

	if l.poll.fd != 0 {
		l.poll.close()
	}
	for i := range l.queues {
		if err := l.queues[i].ring.CloseEventFd(); err != nil && firstErr == nil {
			firstErr = err
		}
		if err := l.queues[i].ring.Close(); err != nil && firstErr == nil {
			firstErr = err
		}
	}
	return firstErr
}

func assert(x bool, msg string, args ...interface{}) {
	if !x {
		panic(fmt.Sprintf(msg, args...))
	}
}
