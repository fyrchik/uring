package loop

import (
	"testing"

	"codeberg.org/fyrchik/uring"
	"github.com/stretchr/testify/require"
)

func TestPoll(t *testing.T) {
	ring, err := uring.Setup(64, nil)
	require.NoError(t, err)
	defer ring.Close()

	var pl poll
	require.NoError(t, pl.init(1))
	defer pl.close()

	require.NoError(t, ring.SetupEventFd())
	defer ring.CloseEventFd()
	require.NoError(t, pl.addRead(int32(ring.EventFd())))

	for i := uint64(1); i < 100; i++ {
		sq := ring.Submission()
		for j := 0; j < 3; j++ {
			var sqe uring.SQEntry
			uring.Nop(&sqe)
			sqe.SetUserData(i)
			require.True(t, sq.Push(sqe), "queue is full")
		}
		sq.Sync()

		submitted, err := ring.Submit()
		require.NoError(t, err)
		require.EqualValues(t, 3, submitted)
		require.NoError(t, pl.wait(func(efd int32) {
			require.Equal(t, int32(ring.EventFd()), efd)
		}))

		cq := ring.Completion()
		for j := 0; j < 3; j++ {
			cqe, ok := cq.GetEntry()
			require.True(t, ok)
			require.Equal(t, i, cqe.UserData())
		}
	}
}
