package uring

import (
	"fmt"
	"os"
	"path/filepath"
	"syscall"
	"testing"

	"github.com/stretchr/testify/require"
	"golang.org/x/sys/unix"
)

func tempFile(t *testing.T) *os.File {
	f, err := os.CreateTemp(t.TempDir(), "*")
	require.NoError(t, err)
	t.Cleanup(func() { _ = f.Close() })
	return f
}

func testFileWriteRead(t *testing.T, r *Ring) {
	f := tempFile(t)
	writeRead(t, r, f.Fd(), f.Fd(), 0)
}

func testFileWritevReadv(t *testing.T, r *Ring) {
	f := tempFile(t)
	writevReadv(t, r, f.Fd(), f.Fd())
}

func testFileWriteReadFileFixed(t *testing.T, r *Ring) {
	f := tempFile(t)
	require.NoError(t, r.RegisterFiles([]int32{int32(f.Fd())}))

	writeRead(t, r, 0, 0, IOSQE_FIXED_FILE)
}

func testFileWriteReadFixed(t *testing.T, r *Ring) {
	f := tempFile(t)

	buf0 := make([]byte, 64)
	buf1 := make([]byte, 64)
	require.NoError(t, r.RegisterBuffers([]syscall.Iovec{
		toIovec(buf0),
		toIovec(buf1),
	}))
	defer func() { require.NoError(t, r.UnregisterBuffers()) }()

	expected := []byte("The quick brown fox jumps over the lazy dog.")

	// Buffer is unregistered, the operation should fail.
	var sqeW SQEntry
	WriteFixed(&sqeW, f.Fd(), expected, 0, 0)
	sqeW.SetUserData(0xF0)

	sq := r.Submission()
	require.True(t, sq.Push(sqeW), "queue is full")
	r.checkSubmit(t, 1, 1)
	r.Completion().expect(t, 0xF0, -int32(syscall.EFAULT))

	copy(buf0, expected)
	expected = buf0[:len(expected)]
	actual := buf1[:len(expected)]

	// OK case.
	sqeW.Reset()
	WriteFixed(&sqeW, f.Fd(), expected, 0, 0)
	sqeW.SetUserData(0xF1)
	sqeW.SetFlags(IOSQE_IO_LINK)

	var sqeR SQEntry
	ReadFixed(&sqeR, f.Fd(), actual, 0, 1)
	sqeR.SetUserData(0xF2)

	require.True(t, sq.Push(sqeW), "queue is full")
	require.True(t, sq.Push(sqeR), "queue is full")
	r.checkSubmit(t, 2, 2)

	cqes := make([]CQEntry, 2)
	require.Equal(t, 2, r.Completion().Fill(cqes))
	require.EqualValues(t, 0xF1, cqes[0].UserData())
	require.EqualValues(t, 0xF2, cqes[1].UserData())
	require.EqualValues(t, len(expected), cqes[0].Res())
	require.EqualValues(t, len(actual), cqes[1].Res())
	require.Equal(t, expected, actual)
}

func testFallocate(t *testing.T, r *Ring) {
	f := tempFile(t)

	var sqe SQEntry
	Fallocate(&sqe, f.Fd(), 0, 10, 32)
	sqe.SetUserData(0x33)

	sq := r.Submission()
	require.True(t, sq.Push(sqe))

	r.checkSubmit(t, 1, 1)

	r.Completion().expect(t, 0x33, 0)

	info, err := f.Stat()
	require.NoError(t, err)
	require.EqualValues(t, 42, info.Size())
}

func testFsync(t *testing.T, r *Ring) {
	f := tempFile(t)
	n, err := f.Write([]byte{1})
	require.NoError(t, err)
	require.Equal(t, 1, n)

	var sqe SQEntry
	Fsync(&sqe, f.Fd(), 0)
	sqe.SetUserData(0x03)

	require.True(t, r.Submission().Push(sqe))
	r.checkSubmit(t, 1, 1)
	r.Completion().expect(t, 0x03, 0)
}

func testSyncFileRange(t *testing.T, r *Ring) {
	f := tempFile(t)
	buf := make([]byte, 3*1024)
	for i := range buf {
		buf[i] = 0x2
	}
	n, err := f.Write(buf)
	require.NoError(t, err)
	require.Equal(t, len(buf), n)

	buf = make([]byte, 1024)
	for i := range buf {
		buf[i] = 0x3
	}
	n, err = f.Write(buf)
	require.NoError(t, err)
	require.Equal(t, len(buf), n)

	var sqe SQEntry
	SyncFileRange(&sqe, f.Fd(), 3*1024, 1024, 0)
	sqe.SetUserData(0x04)

	require.True(t, r.Submission().Push(sqe))
	r.checkSubmit(t, 1, 1)
	r.Completion().expect(t, 0x04, 0)
}

func testOpenat(t *testing.T, r *Ring) {
	f, err := os.Open(t.TempDir())
	require.NoError(t, err)
	defer f.Close()

	p := "myfile"
	_p0, err := syscall.BytePtrFromString(p)
	require.NoError(t, err)

	var sqe SQEntry
	Openat(&sqe, int32(f.Fd()), _p0, uint32(os.O_CREATE), uint32(os.ModePerm))
	sqe.SetUserData(0x36)

	require.True(t, r.Submission().Push(sqe))

	r.checkSubmit(t, 1, 1)
	cq := r.Completion()
	cqe, ok := cq.GetEntry()
	require.True(t, ok, "completion queue is empty")
	require.EqualValues(t, 0x36, cqe.UserData(), "unexpected userData")
	require.True(t, cqe.Res() > 0, "unexpected result")
	cq.Sync() // Notify kernel of consumed CQE.

	var sqeC SQEntry
	Close(&sqeC, uintptr(cqe.Res()))
	sqeC.SetUserData(0x37)

	require.True(t, r.Submission().Push(sqeC))
	r.checkSubmit(t, 1, 1)
	r.Completion().expect(t, 0x37, 0)
}

func testOpenat2(t *testing.T, r *Ring) {
	f, err := os.Open(t.TempDir())
	require.NoError(t, err)
	defer f.Close()

	p := "myfile"
	_p0, err := syscall.BytePtrFromString(p)
	require.NoError(t, err)

	how := unix.OpenHow{
		Flags: uint64(os.O_CREATE),
		Mode:  uint64(os.ModePerm),
	}

	var sqe SQEntry
	Openat2(&sqe, int32(f.Fd()), _p0, how)
	sqe.SetUserData(0x38)

	require.True(t, r.Submission().Push(sqe))

	r.checkSubmit(t, 1, 1)
	cq := r.Completion()
	cqe, ok := cq.GetEntry()
	require.True(t, ok, "completion queue is empty")
	require.EqualValues(t, 0x38, cqe.UserData(), "unexpected userData")
	require.True(t, cqe.Res() > 0, "unexpected result")
	cq.Sync() // Notify kernel of consumed CQE.

	var sqeC SQEntry
	Close(&sqeC, uintptr(cqe.Res()))
	sqeC.SetUserData(0x39)

	require.True(t, r.Submission().Push(sqeC))
	r.checkSubmit(t, 1, 1)
	r.Completion().expect(t, 0x39, 0)
}

func testOpenat2FileIndex(t *testing.T, r *Ring) {
	require.NoError(t, r.UnregisterFiles())
	require.NoError(t, r.RegisterFilesSparse(2))

	// One more round than table size.
	for round := 0; round < 3; round++ {
		dir := filepath.Join(t.TempDir(), fmt.Sprintf("file-%d", round))
		_p0, err := syscall.BytePtrFromString(dir)
		require.NoError(t, err)

		var sqe SQEntry
		Openat2Direct(&sqe, -1, _p0, unix.OpenHow{Flags: uint64(os.O_CREATE)}, -1)
		sqe.SetUserData(0x11)
		require.True(t, r.Submission().Push(sqe))

		r.checkSubmit(t, 1, 1)
		cq := r.Completion()
		cqe, ok := cq.GetEntry()
		require.True(t, ok, "completion queue is empty")
		require.EqualValues(t, 0x11, cqe.UserData(), "unexpected userData")
		if round == 2 {
			require.EqualValues(t, -int32(syscall.ENFILE), cqe.Res(), "expected EINFILE")
		} else {
			require.True(t, cqe.Res() >= 0, "%s", syscall.Errno(-cqe.Res()))
			require.EqualValues(t, round, cqe.Res(), "expected descriptors to be allocated from 0")
		}
		cq.Sync() // Notify kernel of consumed CQE.
	}

	for round := 0; round < 2; round++ {
		var sqeC SQEntry
		CloseDirect(&sqeC, int32(round))
		sqeC.SetUserData(0x12)

		require.True(t, r.Submission().Push(sqeC))
		r.checkSubmit(t, 1, 1)
		r.Completion().expect(t, 0x12, 0)
	}
}

func testPipeSplice(t *testing.T, r *Ring) {
	pr, pw, err := os.Pipe()
	require.NoError(t, err)

	text := []byte("The quick brown fox jumps over the lazy dog.")

	fname := filepath.Join(t.TempDir(), "uring-test-splice")
	require.NoError(t, os.WriteFile(fname, text, os.ModePerm))

	f, err := os.Open(fname)
	require.NoError(t, err)
	defer f.Close()

	var sqe SQEntry
	Splice(&sqe, f.Fd(), 4, pw.Fd(), -1, 10, 0)
	sqe.SetUserData(0x70)

	require.True(t, r.Submission().Push(sqe))
	r.checkSubmit(t, 1, 1)
	r.Completion().expect(t, 0x70, 10)

	actual := make([]byte, len(text))
	n, err := pr.Read(actual)
	require.NoError(t, err)
	require.Equal(t, 10, n)
	require.Equal(t, text[4:14], actual[:n])
}

func makeTestPipe(t *testing.T) (r *os.File, w *os.File) {
	pr, pw, err := os.Pipe()
	require.NoError(t, err)
	t.Cleanup(func() {
		require.NoError(t, pw.Close())
		require.NoError(t, pr.Close())
	})
	return pr, pw
}

func testPipeTee(t *testing.T, r *Ring) {
	// pw ---- pr
	//     |-- pw1 -- pr1
	pr, pw := makeTestPipe(t)
	pr1, pw1 := makeTestPipe(t)

	text := []byte("The quick brown fox jumps over the lazy dog.")
	_, err := pw.Write(text)
	require.NoError(t, err)

	var sqe SQEntry
	Tee(&sqe, pr.Fd(), pw1.Fd(), 10, 0)
	sqe.SetUserData(0x70)

	require.True(t, r.Submission().Push(sqe))
	r.checkSubmit(t, 1, 1)
	r.Completion().expect(t, 0x70, 10)

	actual := make([]byte, len(text))
	n, err := pr.Read(actual)
	require.NoError(t, err)
	require.Equal(t, len(text), n)
	require.Equal(t, text, actual)

	actual = make([]byte, len(text))
	n, err = pr1.Read(actual)
	require.NoError(t, err)
	require.Equal(t, 10, n)
	require.Equal(t, text[:10], actual[:n])
}

func testRegisterFilesUpdate(t *testing.T, r *Ring) {
	f := tempFile(t)

	require.NoError(t, r.UnregisterFiles())
	require.NoError(t, r.RegisterFilesSparse(1))
	require.NoError(t, r.RegisterFilesUpdate(0, []int32{int32(f.Fd())}))
	writeRead(t, r, 0, 0, IOSQE_FIXED_FILE)
}
