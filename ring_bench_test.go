package uring

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func BenchmarkUring(b *testing.B) {
	const ringSize = 16

	r, err := Setup(ringSize, &Params{})
	require.NoError(b, err)
	b.Cleanup(func() { require.NoError(b, r.Close()) })

	cqes := make([]CQEntry, ringSize)
	nop := func() SQEntry {
		var sqe SQEntry
		Nop(&sqe)
		return sqe
	}

	b.ResetTimer()
	b.ReportAllocs()
	for i := 0; i < b.N; i++ {
		count := 128
		for count != 0 {
			sq := r.Submission()
			for count != 0 {
				if sq.Push(nop()) {
					count -= 1
				} else {
					break
				}
			}
			sq.Sync()

			n, err := r.SubmitAndWait(ringSize)
			if err != nil || n != ringSize {
				b.Fatalf("submitted: %d, error: %v", n, err)
			}

			cq := r.Completion()
			cq.Fill(cqes)
			cq.Sync()
		}
	}
}
