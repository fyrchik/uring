package uring

import (
	"fmt"
	"sync/atomic"
	"unsafe"
)

// CQEntry represents completion queue entry.
type CQEntry struct {
	userData uint64
	res      int32
	flags    FlagCQE
}

// FlagSQE is the type for opcode-specifig CQE flags.
type FlagCQE uint32

// Completion queue entry flags.
const (
	IORING_CQE_F_BUFFER FlagCQE = 1 << iota
	IORING_CQE_F_MORE
	IORING_CQE_F_SOCK_NONEMPTY
	IORING_CQE_F_NOTIF
)

// IORING_CQE_BUFFER_SHIFT is the first bit of buffer id, ff IORING_CQE_BUFFER is set.
const IORING_CQE_BUFFER_SHIFT = 16

// UserData returns CQE userData field which matches userData for the corresponding SQE.
func (c CQEntry) UserData() uint64 {
	return c.userData
}

// Res returns operation result.
func (c CQEntry) Res() int32 {
	return c.res
}

// Flags returns CQE flags.
func (c CQEntry) Flags() FlagCQE {
	return c.flags
}

// CompletionQueue represents completion queue.
// It is NOT thread-safe and stores head/tail pointers in non-atomic fields.
type CompletionQueue struct {
	head uint32
	tail uint32
	cQueueInner
}

// cQueueInner contains fields filled by the kernel.
type cQueueInner struct {
	data      []byte
	kHead     *uint32
	kTail     *uint32
	kOverflow *uint32
	kFlags    *uint32

	ringMask    uint32
	ringEntries uint32
	cqes        uintptr
}

func (c *cQueueInner) init(cqData []byte, off cqRingOffsets) {
	c.data = cqData

	pointer := unsafe.Pointer(&cqData[0])
	c.kHead = (*uint32)(unsafe.Add(pointer, off.Head))
	c.kTail = (*uint32)(unsafe.Add(pointer, off.Tail))
	c.ringMask = *(*uint32)(unsafe.Add(pointer, off.RingMask))
	c.ringEntries = *(*uint32)(unsafe.Add(pointer, off.RingEntries))
	c.kOverflow = (*uint32)(unsafe.Add(pointer, off.Overflow))
	c.cqes = uintptr(unsafe.Add(pointer, off.CQEs))
	if off.Flags != 0 {
		c.kFlags = (*uint32)(unsafe.Add(pointer, off.Flags))
	}
}

// Sync synchronizes completion queue with the kernel.
//
// It flushes any consumed entries and makes available new entries produced by the kernel.
func (cq *CompletionQueue) Sync() {
	atomic.StoreUint32(cq.kHead, cq.head)
	cq.tail = atomic.LoadUint32(cq.kTail)
}

// Len returns the number of available completion queue entries.
func (cq *CompletionQueue) Len() uint32 {
	return cq.tail - cq.head
}

// Full returns true if cq is full.
func (cq *CompletionQueue) Full() bool {
	return cq.Len() == cq.ringEntries
}

// Empty returns true iff cq is empty.
func (cq *CompletionQueue) Empty() bool {
	return cq.Len() == 0
}

// Fill is like `GetEntry` but for multiple fetching multiple entries.
// Returns the amount of entries read.
// Note that `Sync` MUST be called after to notify kernel about read CQEs.
func (cq *CompletionQueue) Fill(entries []CQEntry) int {
	size := int(cq.Len())
	if len(entries) < size {
		size = len(entries)
	}

	for i := 0; i < size; i++ {
		index := cq.head & cq.ringMask
		entries[i] = *(*CQEntry)(unsafe.Pointer(cq.cqes + uintptr(index)*cqeSize))
		cq.head++
	}
	return size
}

func (cq *CompletionQueue) Dump(arg string) {
	fmt.Printf("%s CQ State: head=%d tail=%d\n", arg, cq.head, cq.tail)
}

// GetEntry returns completion queue entry if it is available.
// Second return value signifies success.
// Note that `Sync` MUST be called after to notify kernel about read CQEs.
func (cq *CompletionQueue) GetEntry() (CQEntry, bool) {
	if cq.head != cq.tail {
		index := cq.head & cq.ringMask
		entry := (*CQEntry)(unsafe.Pointer(cq.cqes + uintptr(index)*cqeSize))
		cq.head++
		return *entry, true
	}
	return CQEntry{}, false
}
