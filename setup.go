package uring

import (
	"fmt"
	"math/bits"
	"syscall"
	"unsafe"
)

// Ring represents uring instance.
type Ring struct {
	fd      int
	eventfd uintptr
	flags   FlagSetup
	sq      SubmissionQueue
	cq      CompletionQueue
	params  Params
}

type sqRingOffsets struct {
	Head        uint32
	Tail        uint32
	RingMask    uint32
	RingEntries uint32
	Flags       uint32
	Dropped     uint32
	Array       uint32
	Resv1       uint32
	Resv2       uint64
}

type cqRingOffsets struct {
	Head        uint32
	Tail        uint32
	RingMask    uint32
	RingEntries uint32
	Overflow    uint32
	CQEs        uint32
	Flags       uint32
	Resv1       uint32
	Resv2       uint64
}

type Params struct {
	SQEntries    uint32 // filled by kernel
	CQEntries    uint32 // filled by kernel or can be set with IORING_SETUP_CQSIZE flag.
	Flags        FlagSetup
	SQThreadCPU  uint32
	SQThreadIdle uint32
	Features     uint32
	WQFd         uint32
	_reserved    [3]uint32
	sqOff        sqRingOffsets
	cqOff        cqRingOffsets
}

const (
	SYS_IO_URING_SETUP uintptr = 425 + iota
	SYS_IO_URING_ENTER
	SYS_IO_URING_REGISTER
)

const (
	IORING_OFF_SQ_RING = 0
	IORING_OFF_CQ_RING = 0x8000000
	IORING_OFF_SQES    = 0x10000000
)

// Setup perfoms `io_uring_setup` system call.
func Setup(size int, p *Params) (*Ring, error) {
	var r Ring
	if p != nil {
		r.params = *p
	}
	if err := setup(size, &r, &r.params); err != nil {
		return nil, err
	}
	return &r, nil
}

func setup(size int, r *Ring, p *Params) error {
	fd, _, errno := syscall.Syscall(SYS_IO_URING_SETUP, uintptr(size), uintptr(unsafe.Pointer(p)), 0)
	if errno != 0 {
		return fmt.Errorf("IO_URING_SETUP %w", error(errno))
	}

	if errno := uringQueueMmap(int(fd), p, r); errno != nil {
		_ = syscall.Close(int(fd))
		return errno
	}

	/*
	 * Directly map SQ slots to SQEs
	 */
	for i := uint32(0); i < r.sq.ringEntries; i++ {
		*(*uint32)(unsafe.Pointer(r.sq.array + uintptr(i)*4)) = i
	}
	return nil
}

func uringQueueMmap(fd int, p *Params, r *Ring) error {
	err := uringMmap(fd, p, &r.sq, &r.cq)
	if err != nil {
		_ = r.Close()
		return err
	}

	r.flags = p.Flags
	r.fd = fd
	return nil
}

func uringMmap(fd int, p *Params, sq *SubmissionQueue, cq *CompletionQueue) error {
	sqSize := p.sqOff.Array + p.SQEntries*(bits.UintSize/8) // sizeof(unsigned)
	cqSize := p.cqOff.CQEs + p.CQEntries*cqeSize
	singleMap := p.Features&IORING_FEAT_SINGLE_MMAP != 0
	if singleMap {
		if cqSize > sqSize {
			sqSize = cqSize
		}
		cqSize = sqSize
	}

	sqData, err := mmap(fd, IORING_OFF_SQ_RING, int(sqSize))
	if err != nil {
		return err
	}

	cqData := sqData
	if !singleMap {
		cqData, err = mmap(fd, IORING_OFF_CQ_RING, int(cqSize))
		if err != nil {
			return err
		}
	}

	sqes, err := mmap(fd, IORING_OFF_SQES, sqeSize*int(p.SQEntries))
	if err != nil {
		return err
	}

	sq.init(sqData, sqes, p.sqOff)
	cq.init(cqData, p.cqOff)

	return nil
}

func mmap(fd int, offset int64, length int) ([]byte, error) {
	return syscall.Mmap(fd, offset, length,
		syscall.PROT_READ|syscall.PROT_WRITE,
		syscall.MAP_SHARED|syscall.MAP_POPULATE)
}
