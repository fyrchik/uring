package uring

import (
	"sync/atomic"
	"unsafe"
)

// SQEntry represents submission queue entry.
type SQEntry struct {
	opcode      Opcode //
	flags       FlagSQE
	priority    uint16
	fd          int32
	offset      uint64 // liburing union: off, addr2, cmd_op + __pad1
	addr        uint64 // liburing union: addr, splice_off_in
	len         uint32
	opcodeFlags uint32
	userData    uint64
	bufIndex    uint16 // liburing union: buf_index, buf_group
	personality uint16
	_pad3       uint32 // liburing union: splice_fd_in, file_index, addr_len + __pad3[1]
	addr3       uint64
	_pad2       [1]uint64
}

// SetUserData sets userData field of e.
func (e *SQEntry) SetUserData(v uint64) {
	e.userData = v
}

// SetUserData sets SQE flags of e.
func (e *SQEntry) SetFlags(f FlagSQE) {
	e.flags |= f
}

// Reset zeroes e.
func (e *SQEntry) Reset() {
	*e = SQEntry{}
}

// Submission queue ring flags.
const (
	IORING_SQ_NEED_WAKEUP uint32 = 1 << iota // needs io_uring_enter wakeup
	IORING_SQ_CQ_OVERFLOW                    // CQ ring is overflown
	IORING_SQ_TASKRUN                        // task should enter the kernel
)

// SubmissionQueue represents submission queue.
// It is NOT thread-safe and stores head/tail pointers in non-atomic fields.
type SubmissionQueue struct {
	head uint32
	tail uint32
	sQueueInner
}

// sQueueInner contains fields filled by the kernel.
type sQueueInner struct {
	kHead       *uint32
	kTail       *uint32
	kFlags      *uint32
	kDropped    *uint32
	ringMask    uint32
	ringEntries uint32

	arrayData []byte
	array     uintptr

	sqesData []byte
	sqes     uintptr
}

func (sq *SubmissionQueue) init(data []byte, sqes []byte, off sqRingOffsets) {
	pointer := unsafe.Pointer(&data[0])

	sq.arrayData = data
	sq.sqesData = sqes
	sq.kHead = (*uint32)(unsafe.Add(pointer, off.Head))
	sq.kTail = (*uint32)(unsafe.Add(pointer, off.Tail))
	sq.ringMask = *(*uint32)(unsafe.Add(pointer, off.RingMask))
	sq.ringEntries = *(*uint32)(unsafe.Add(pointer, off.RingEntries))
	sq.kFlags = (*uint32)(unsafe.Add(pointer, off.Flags))
	sq.kDropped = (*uint32)(unsafe.Add(pointer, off.Dropped))
	sq.array = uintptr(unsafe.Add(pointer, off.Array))
	sq.sqes = uintptr(unsafe.Pointer(&sqes[0]))
}

// Sync synchronizes submission queue with the kernel.
//
// It flushes any added entries and updates queue size if kernel has consumed any.
func (sq *SubmissionQueue) Sync() {
	atomic.StoreUint32(sq.kTail, sq.tail)
	sq.head = atomic.LoadUint32(sq.kHead)
}

// Len returns the number of filled submission queue entries.
func (sq *SubmissionQueue) Len() uint32 {
	return sq.tail - sq.head
}

// Full returns true iff sq is full.
func (sq *SubmissionQueue) Full() bool {
	return sq.Len() == sq.ringEntries
}

// Empty returns true iff sq is empty.
func (sq *SubmissionQueue) Empty() bool {
	return sq.Len() == 0
}

// Capacity returns submission queue capacity.
func (sq *SubmissionQueue) Capacity() uint32 {
	return sq.ringEntries
}

// Push pushes new entry to sq. Returns true on success and false if the queue is full.
// Note, that it does not notify the kernel about new SQEs, so `Sync` must be called.
func (sq *SubmissionQueue) Push(entry SQEntry) bool {
	if sq.Full() {
		return false
	}

	index := sq.tail & sq.ringMask
	*(*SQEntry)(unsafe.Pointer(sq.sqes + uintptr(index)*sqeSize)) = entry
	sq.tail = sq.tail + 1
	return true
}

// PushMany pushes multiple entries to sq. Returns true on success and false if the queue has not enough free slots.
// This is usually needed to chain multiple operations with IOSQE_IO_LINK flag. Entries are guaranteed to take successive slots in the queue.
// Note, that it does not notify the kernel about new SQEs, so `Sync` must be called.
func (sq *SubmissionQueue) PushMany(entries []SQEntry) bool {
	if sq.ringEntries-sq.Len() < uint32(len(entries)) {
		return false
	}

	for i := range entries {
		index := sq.tail & sq.ringMask
		*(*SQEntry)(unsafe.Pointer(sq.sqes + uintptr(index)*sqeSize)) = entries[i]
		sq.tail = sq.tail + 1
	}
	return true
}
