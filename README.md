# IO uring with Go

This is based on:
1. https://github.com/dshulyak/uring
2. https://github.com/tokio-rs/io-uring

## Package structure
- `uring` package contains low-level functions for using uring directly
- `loop` package contains simple event loop on top of uring
