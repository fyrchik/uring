package uring

import (
	"syscall"
)

const (
	sqeSize = 64
	cqeSize = 16
)

// FlagSetup is a type for `io_uring_setup` syscall flags.
type FlagSetup uint32

// Setup flags.
const (
	IORING_SETUP_IOPOLL FlagSetup = 1 << iota // io_context is polled
	IORING_SETUP_SQPOLL
	IORING_SETUP_SQ_AFF
	IORING_SETUP_CQSIZE
	IORING_SETUP_CLAMP
	IORING_SETUP_ATTACH_WQ
	IORING_SETUP_R_DISABLED
	IORING_SETUP_SUBMIT_ALL
	IORING_SETUP_COOP_TASKRUN
	IORING_SETUP_TASKRUN_FLAG
	IORING_SETUP_SQE128
	IORING_SETUP_CQE32
	IORING_SETUP_SINGLE_ISSUER
	IORING_SETUP_DEFER_TASKRUN
)

// Features.
const (
	IORING_FEAT_SINGLE_MMAP = 1 << iota
	IORING_FEAT_NODROP
	IORING_FEAT_SUBMIT_STABLE
	IORING_FEAT_RW_CUR_POS
	IORING_FEAT_CUR_PERSONALITY
	IORING_FEAT_FAST_POLL
	IORING_FEAT_POLL_32BITS
	IORING_FEAT_SQPOLL_NONFIXED
	IORING_FEAT_EXT_ARG
	IORING_FEAT_NATIVE_WORKERS
	IORING_FEAT_RSRC_TAGS
	IORING_FEAT_CQE_SKIP
	IORING_FEAT_LINKED_FILE
	IORING_FEAT_REG_REG_RING
)

// Completion returns completion queue.
func (r *Ring) Completion() *CompletionQueue {
	r.cq.Sync()
	return &r.cq
}

// Submission returns submission queue.
func (r *Ring) Submission() *SubmissionQueue {
	r.sq.Sync()
	return &r.sq
}

// SQSize returns submission queue size.
func (r *Ring) SQSize() uint32 {
	return r.params.SQEntries
}

// CQSize returns completion queue size.
func (r *Ring) CQSize() uint32 {
	return r.params.CQEntries
}

// Fd return uring fd.
func (r *Ring) Fd() uintptr {
	return uintptr(r.fd)
}

// Close unmaps all queues, closes ring fd and releases all other resources.
func (r *Ring) Close() (err error) {
	if r.cq.data != nil {
		ret := syscall.Munmap(r.cq.data)
		if err == nil {
			err = ret
		}
		if ret == nil {
			r.cq.data = nil
		}
	}
	if r.sq.sqesData != nil {
		ret := syscall.Munmap(r.sq.sqesData)
		if err == nil {
			err = ret
		}
		if ret == nil {
			r.sq.sqesData = nil
		}
	}
	if r.sq.arrayData != nil && r.params.Features&IORING_FEAT_SINGLE_MMAP == 0 {
		ret := syscall.Munmap(r.sq.arrayData)
		if err == nil {
			err = ret
		}
		if ret == nil {
			r.sq.arrayData = nil
		}
	}
	if r.fd != 0 {
		ret := syscall.Close(r.fd)
		if err == nil {
			err = ret
		}
		if ret == nil {
			r.fd = 0
		}
	}
	return
}
