package uring

import (
	"testing"
	"unsafe"

	"github.com/stretchr/testify/require"
)

func TestSizes(t *testing.T) {
	require.EqualValues(t, sqeSize, unsafe.Sizeof(SQEntry{}))
	require.EqualValues(t, cqeSize, unsafe.Sizeof(CQEntry{}))
}

func TestUring(t *testing.T) {
	c := newRingTestContext(t)

	t.Run("queue", func(t *testing.T) {
		c.run(t, "Nop", testNop)
		c.run(t, "Splice", testQueues)
	})
	t.Run("file", func(t *testing.T) {
		c.run(t, "WriteRead", testFileWriteRead)
		c.run(t, "WritevReadv", testFileWritevReadv)
		c.run(t, "WriteReadFileFixed", testFileWriteReadFileFixed)
		c.run(t, "WriteReadFixed", testFileWriteReadFixed)
		c.run(t, "Fallocate", testFallocate)
		c.run(t, "Fsync", testFsync)
		c.run(t, "SyncFileRange", testSyncFileRange)
		c.run(t, "Openat", testOpenat)
		c.run(t, "Openat2", testOpenat2)
		c.run(t, "Openat2FileIndex", testOpenat2FileIndex)
		c.run(t, "RegisterFilesSparseUpdate", testRegisterFilesUpdate)
		c.run(t, "Splice", testPipeSplice)
		c.run(t, "Tee", testPipeTee)
	})
	t.Run("network", func(t *testing.T) {
		c.run(t, "WriteRead", testNetWriteRead)
		c.run(t, "WritevReadv", testNetWritevReadv)
		c.run(t, "SendRecv", testNetSendRecv)
		c.run(t, "SendzcRecv", testNetSendzcRecv)
		c.run(t, "SendzcFixedRecv", testNetSendzcFixedRecv)
		c.run(t, "SendMsgRecvMsg", testNetSendmsgRecvmsg)
		c.run(t, "SendMsgzcRecv", testNetSendmsgzcRecvmsg)
		c.run(t, "Accept", testNetAccept)
		c.run(t, "Connect", testNetConnect)
	})
	t.Run("timeout", func(t *testing.T) {
		c.run(t, "Simple", testTimeoutSimple)
		c.run(t, "WithNop", testTimeoutAndNop)
		c.run(t, "Count", testTimeoutCount)
		c.run(t, "Remove", testTimeoutRemove)
		c.run(t, "Cancel", testTimeoutCancel)
		c.run(t, "Absolute", testTimeoutAbsolute)
		c.run(t, "LinkedCanceled", testLinkTimeoutCanceled)
		c.run(t, "LinkedFired", testLinkTimeoutFired)
	})
}
