package uring

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func testNop(t *testing.T, r *Ring) {
	r.Submission().pushNop(t, 42)
	r.checkSubmit(t, 1, 1)
	r.Completion().expect(t, 42, 0)
}

func testQueues(t *testing.T, r *Ring) {
	sq := r.Submission()
	cq := r.Completion()

	require.True(t, sq.Empty())

	for i := 0; i < int(sq.Capacity()); i++ {
		var sqe SQEntry
		Nop(&sqe)
		require.True(t, sq.Push(sqe), "queue is full")
	}

	require.True(t, sq.Full())

	sq.Sync()

	submitted, err := r.Submit()
	require.NoError(t, err)
	require.Equal(t, sq.Capacity(), submitted)

	require.True(t, sq.Full())
	sq.Sync()
	require.True(t, sq.Empty())

	require.True(t, cq.Empty())
	cq.Sync()

	ln := cq.Len()
	require.Equal(t, ln, sq.Capacity())

	for i := uint32(0); i < ln; i++ {
		_, ok := cq.GetEntry()
		require.True(t, ok)
	}
}
