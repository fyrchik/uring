package uring

import (
	"net"
	"syscall"
	"testing"

	"codeberg.org/fyrchik/uring/netutil"
	"github.com/stretchr/testify/require"
)

func tcpListener(t *testing.T, addr *net.TCPAddr) *net.TCPListener {
	if addr == nil {
		addr = &net.TCPAddr{IP: net.IPv4(127, 0, 0, 1)}
	}

	l, err := net.ListenTCP("tcp", addr)
	require.NoError(t, err)
	t.Cleanup(func() { _ = l.Close })

	return l
}

func testTCPPair(t *testing.T) (*net.TCPConn, *net.TCPConn) {
	addr := &net.TCPAddr{
		IP: net.IPv4(127, 0, 0, 1),
	}

	l, err := net.ListenTCP("tcp", addr)
	require.NoError(t, err)
	t.Cleanup(func() { _ = l.Close() })

	send, err := net.DialTCP("tcp", nil, l.Addr().(*net.TCPAddr))
	require.NoError(t, err)
	t.Cleanup(func() { _ = send.Close() })

	recv, err := l.AcceptTCP()
	require.NoError(t, err)
	t.Cleanup(func() { _ = recv.Close() })

	return send, recv
}

func toSyscallConn(t *testing.T, conn syscall.Conn) syscall.RawConn {
	r, err := conn.SyscallConn()
	require.NoError(t, err)
	return r
}

func testNetSendRecv(t *testing.T, r *Ring) {
	testWithTCP(t, func(t *testing.T, send, recv uintptr) {
		sendRecv(t, r, send, recv, 0)
	})
}

func testNetSendzcRecv(t *testing.T, r *Ring) {
	testWithTCP(t, func(t *testing.T, send, recv uintptr) {
		expected := []byte("The quick brown fox jumps over the lazy dog.")
		actual := make([]byte, len(expected))

		var sqeS SQEntry
		SendZc(&sqeS, send, expected, 0, 0)
		sqeS.SetUserData(1)
		sqeS.SetFlags(IOSQE_IO_LINK)

		var sqeR SQEntry
		Recv(&sqeR, recv, actual, 0)
		sqeR.SetUserData(2)

		sq := r.Submission()
		require.True(t, sq.PushMany([]SQEntry{sqeS, sqeR}), "queue is full")
		r.checkSubmit(t, 2, 2)

		cqes := make([]CQEntry, 3)
		require.Equal(t, 3, r.Completion().Fill(cqes))
		require.EqualValues(t, 1, cqes[0].UserData())
		require.EqualValues(t, len(expected), cqes[0].Res())
		require.True(t, cqes[0].Flags()&IORING_CQE_F_MORE != 0, "expected IORING_CQE_F_MORE to be present")

		switch cqes[1].UserData() {
		case 1:
			require.True(t, cqes[1].Flags()&IORING_CQE_F_MORE == 0)
			require.EqualValues(t, cqes[2].UserData(), 2)
			require.EqualValues(t, cqes[2].Res(), len(expected))
		case 2:
			require.EqualValues(t, cqes[2].UserData(), 1)
			require.True(t, cqes[2].Flags()&IORING_CQE_F_MORE == 0)
			require.EqualValues(t, cqes[1].Res(), len(expected))
		default:
			require.Fail(t, "unexpected user data: %d", cqes[1].UserData())
		}
		require.Equal(t, expected, actual)
	})
}

func testNetSendzcFixedRecv(t *testing.T, r *Ring) {
	testWithTCP(t, func(t *testing.T, send, recv uintptr) {
		// Just to make things more interesting.
		const expectedStr = "The quick brown fox jumps over the lazy dog."
		const offset = 7
		buf := make([]byte, 100)
		copy(buf[offset:], expectedStr)

		actual := make([]byte, len(expectedStr))

		require.NoError(t, r.RegisterBuffers([]syscall.Iovec{toIovec(buf)}))
		defer func() { require.NoError(t, r.UnregisterBuffers()) }()

		var sqeS SQEntry
		SendZcFixed(&sqeS, send, buf[offset:offset+len(expectedStr)], 0, 0, 0)
		sqeS.SetUserData(1)
		sqeS.SetFlags(IOSQE_IO_LINK)

		var sqeR SQEntry
		Recv(&sqeR, recv, actual, 0)
		sqeR.SetUserData(2)

		sq := r.Submission()
		require.True(t, sq.PushMany([]SQEntry{sqeS, sqeR}), "queue is full")
		r.checkSubmit(t, 2, 2)

		cqes := make([]CQEntry, 3)
		require.Equal(t, 3, r.Completion().Fill(cqes))
		require.EqualValues(t, 1, cqes[0].UserData())
		require.EqualValues(t, len(expectedStr), cqes[0].Res())
		require.True(t, cqes[0].Flags()&IORING_CQE_F_MORE != 0, "expected IORING_CQE_F_MORE to be present")

		switch cqes[1].UserData() {
		case 1:
			require.True(t, cqes[1].Flags()&IORING_CQE_F_MORE == 0)
			require.EqualValues(t, cqes[2].UserData(), 2)
			require.EqualValues(t, cqes[2].Res(), len(expectedStr))
		case 2:
			require.EqualValues(t, cqes[2].UserData(), 1)
			require.True(t, cqes[2].Flags()&IORING_CQE_F_MORE == 0)
			require.EqualValues(t, cqes[1].Res(), len(expectedStr))
		default:
			require.Fail(t, "unexpected user data: %d", cqes[1].UserData())
		}
		require.Equal(t, expectedStr, string(actual))
	})
}

func testNetSendmsgRecvmsg(t *testing.T, r *Ring) {
	sendConn, recvConn := testTCPPair(t)
	sc := toSyscallConn(t, sendConn)
	rc := toSyscallConn(t, recvConn)
	withNetFD(t, sc, rc, func(t *testing.T, send, recv uintptr) {
		expected := []byte("The quick brown fox jumps over the lazy dog.")
		actual := make([]byte, len(expected))

		bufs := []syscall.Iovec{toIovec(expected)}
		bufs2 := []syscall.Iovec{toIovec(actual)}

		sa := netutil.TCPAddrToSockaddr(recvConn.LocalAddr().(*net.TCPAddr))
		ptr, n := sa.Sockaddr()
		msgS := &syscall.Msghdr{
			Name:    (*byte)(ptr),
			Namelen: n,
			Iov:     &bufs[0],
			Iovlen:  1,
		}

		var sqeS SQEntry
		SendMsg(&sqeS, send, msgS, 0)
		sqeS.SetUserData(1)
		sqeS.SetFlags(IOSQE_IO_LINK)

		msgR := &syscall.Msghdr{
			Name:    (*byte)(ptr),
			Namelen: n,
			Iov:     &bufs2[0],
			Iovlen:  1,
		}

		var sqeR SQEntry
		RecvMsg(&sqeR, recv, msgR, 0)
		sqeR.SetUserData(2)

		sq := r.Submission()
		require.True(t, sq.PushMany([]SQEntry{sqeS, sqeR}), "queue is full")
		r.checkSubmit(t, 2, 2)

		cqes := make([]CQEntry, 2)
		require.Equal(t, 2, r.Completion().Fill(cqes))
		require.EqualValues(t, 1, cqes[0].UserData())
		require.EqualValues(t, 2, cqes[1].UserData())
		require.EqualValues(t, len(expected), cqes[0].Res())
		require.EqualValues(t, len(actual), cqes[1].Res())
		require.Equal(t, expected, actual)
	})
}

func testNetSendmsgzcRecvmsg(t *testing.T, r *Ring) {
	sendConn, recvConn := testTCPPair(t)
	sc := toSyscallConn(t, sendConn)
	rc := toSyscallConn(t, recvConn)
	withNetFD(t, sc, rc, func(t *testing.T, send, recv uintptr) {
		expected := []byte("The quick brown fox jumps over the lazy dog.")
		actual := make([]byte, len(expected))

		bufs := []syscall.Iovec{toIovec(expected)}
		bufs2 := []syscall.Iovec{toIovec(actual)}

		sa := netutil.TCPAddrToSockaddr(recvConn.LocalAddr().(*net.TCPAddr))
		ptr, n := sa.Sockaddr()
		msgS := &syscall.Msghdr{
			Name:    (*byte)(ptr),
			Namelen: n,
			Iov:     &bufs[0],
			Iovlen:  1,
		}

		var sqeS SQEntry
		SendMsgZc(&sqeS, send, msgS, 0)
		sqeS.SetUserData(1)
		sqeS.SetFlags(IOSQE_IO_LINK)

		msgR := &syscall.Msghdr{
			Name:    (*byte)(ptr),
			Namelen: n,
			Iov:     &bufs2[0],
			Iovlen:  1,
		}

		var sqeR SQEntry
		RecvMsg(&sqeR, recv, msgR, 0)
		sqeR.SetUserData(2)

		sq := r.Submission()
		require.True(t, sq.PushMany([]SQEntry{sqeS, sqeR}), "queue is full")
		r.checkSubmit(t, 2, 2)

		cqes := make([]CQEntry, 3)
		require.Equal(t, 3, r.Completion().Fill(cqes))
		require.EqualValues(t, 1, cqes[0].UserData())
		require.EqualValues(t, len(expected), cqes[0].Res())
		require.True(t, cqes[0].Flags()&IORING_CQE_F_MORE != 0, "expected IORING_CQE_F_MORE to be present")

		switch cqes[1].UserData() {
		case 1:
			require.True(t, cqes[1].Flags()&IORING_CQE_F_MORE == 0)
			require.EqualValues(t, cqes[2].UserData(), 2)
			require.EqualValues(t, cqes[2].Res(), len(expected))
		case 2:
			require.EqualValues(t, cqes[2].UserData(), 1)
			require.True(t, cqes[2].Flags()&IORING_CQE_F_MORE == 0)
			require.EqualValues(t, cqes[1].Res(), len(expected))
		default:
			require.Fail(t, "unexpected user data: %d", cqes[1].UserData())
		}
		require.Equal(t, expected, actual)
	})
}

func testNetWriteRead(t *testing.T, r *Ring) {
	testWithTCP(t, func(t *testing.T, send, recv uintptr) {
		writeRead(t, r, send, recv, 0)
	})
}

func testNetWritevReadv(t *testing.T, r *Ring) {
	testWithTCP(t, func(t *testing.T, send, recv uintptr) {
		writevReadv(t, r, send, recv)
	})
}

func testNetAccept(t *testing.T, r *Ring) {
	ls := tcpListener(t, nil)
	toSyscallConn(t, ls).Control(func(fd uintptr) {
		var sa netutil.SockaddrInet4
		var sqe SQEntry
		Accept(&sqe, fd, &sa, 0)
		sqe.SetUserData(0x0e)

		_, err := net.DialTCP("tcp", nil, ls.Addr().(*net.TCPAddr))
		require.NoError(t, err)

		require.True(t, r.Submission().Push(sqe))
		r.checkSubmit(t, 1, 1)

		cq := r.Completion()
		cqe, ok := cq.GetEntry()
		require.True(t, ok, "completion queue is empty")
		require.EqualValues(t, 0x0e, cqe.UserData(), "unexpected userData")
		require.True(t, cqe.Res() > 0, "unexpected operation result: %s", syscall.Errno(-cqe.Res()))
		cq.Sync() // Notify kernel of consumed CQE.

		_ = syscall.Close(int(cqe.Res()))
	})
}

func testNetConnect(t *testing.T, r *Ring) {
	ls := tcpListener(t, nil)
	addr := ls.Addr().(*net.TCPAddr)

	sa := &netutil.SockaddrInet4{
		Addr: [4]byte(addr.IP),
		Port: addr.Port,
	}

	fdFrom, err := syscall.Socket(syscall.AF_INET, syscall.SOCK_STREAM, syscall.IPPROTO_TCP)
	require.NoError(t, err)

	var sqe SQEntry
	Connect(&sqe, uintptr(fdFrom), sa)
	sqe.SetUserData(0x0f)

	require.True(t, r.Submission().Push(sqe))
	r.checkSubmit(t, 1, 1)
	r.Completion().expect(t, 0x0f, 0)

	_, err = ls.Accept()
	require.NoError(t, err)
}

func testWithTCP(t *testing.T, f func(t *testing.T, s, r uintptr)) {
	sendConn, recvConn := testTCPPair(t)
	sc := toSyscallConn(t, sendConn)
	rc := toSyscallConn(t, recvConn)
	withNetFD(t, sc, rc, f)
}

func withNetFD(t *testing.T, send, recv syscall.RawConn, f func(t *testing.T, s, r uintptr)) {
	require.NoError(t, send.Control(func(s uintptr) {
		require.NoError(t, recv.Control(func(r uintptr) {
			f(t, s, r)
		}))
	}))
}
